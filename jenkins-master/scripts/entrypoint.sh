#!/bin/bash
set -xe
if [ -z $JENKINS_S3_BUCKET_NAME ]; then
    echo "ERROR: Cannot start Jenkins container without the JENKINS_S3_BUCKET_NAME set."
    exit 1
fi

if [ -z $JENKINS_AGENTS_CLUSTER_ARN ]; then
    echo "ERROR: Cannot start Jenkins container without the JENKINS_AGENTS_CLUSTER_ARN set."
    exit 1
fi

if [ -z $JENKINS_SECURITY_GROUP ]; then
    echo "ERROR: Cannot start Jenkins container without the JENKINS_SECURITY_GROUP set."
    exit 1
fi

if [ -z $JENKINS_SUBNET ]; then
    echo "ERROR: Cannot start Jenkins container without the JENKINS_SUBNET set."
    exit 1
fi

if [ -z $AWS_LOGS_GROUP ]; then
    echo "ERROR: Cannot start Jenkins container without the AWS_LOGS_GROUP set."
    exit 1
fi

if [ -z $AWS_LOGS_REGION ]; then
    echo "ERROR: Cannot start Jenkins container without the AWS_LOGS_REGION set."
    exit 1
fi

if [ -z $AWS_LOGS_PREFIX ]; then
    echo "ERROR: Cannot start Jenkins container without the AWS_LOGS_PREFIX set."
    exit 1
fi

if [ -z $STACK_NAME ]; then
    echo "ERROR: Cannot start Jenkins container without the STACK_NAME set."
    exit 1
fi

if [ -z $AGENT_IMAGE ]; then
    echo "ERROR: Cannot start Jenkins container without the AGENT_IMAGE set."
    exit 1
fi

if [ -z $AWS_REGION ]; then
    echo "ERROR: Cannot start Jenkins container without the AWS_REGION set."
    exit 1
fi

mkdir /root/.ssh
chmod 600 /root/.ssh

aws configure set s3.signature_version s3v4
aws s3api get-object --bucket $JENKINS_S3_BUCKET_NAME --key id_rsa /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

# We need to copy groovy files to jenkins_home mount point
cp /tmp/jenkins-groovy/*.groovy /var/jenkins_home/init.groovy.d/

# Now its time to launch jenkins
/usr/local/bin/jenkins.sh
