import jenkins.model.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.impl.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey

credStore = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()
scope = CredentialsScope.GLOBAL
source = new BasicSSHUserPrivateKey.UsersPrivateKeySource()

credentials = new BasicSSHUserPrivateKey(scope, "git", "jenkins", source, "", "")

credStore.addCredentials(Domain.global(), credentials)
