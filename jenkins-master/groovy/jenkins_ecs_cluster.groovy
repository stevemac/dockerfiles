import jenkins.model.*

import com.cloudbees.jenkins.plugins.amazonecs.ECSTaskTemplate
import com.cloudbees.jenkins.plugins.amazonecs.ECSTaskTemplate.MountPointEntry
import com.cloudbees.jenkins.plugins.amazonecs.ECSTaskTemplate.EnvironmentEntry
import com.cloudbees.jenkins.plugins.amazonecs.ECSTaskTemplate.LogDriverOption
import com.cloudbees.jenkins.plugins.amazonecs.ECSCloud

def env = System.getenv()

def cloudName = env['STACK_NAME'] + "-agents"
def instance = Jenkins.getInstance()
def clouds = instance.clouds
def masterImage = env['AGENT_IMAGE']
def region = env["AWS_REGION"]

instance.setNumExecutors(0)

if (clouds.findIndexOf { it.name == cloudName } == -1) {

  String masterIP = ""

  if (env['OVERRIDE_MASTER_IP'] == null) {
      URL url = new URL("http://169.254.169.254/latest/meta-data/local-ipv4");

      BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))
      masterIP = reader.readLine()
  }
  else {
      masterIP = env['OVERRIDE_MASTER_IP']
  }

  clusterArn = env['JENKINS_AGENTS_CLUSTER_ARN']

  def ecsTemplate = new ECSTaskTemplate(
    templateName="ecs",
    label="ecs base-agent",
    image=masterImage,
    remoteFSRoot=null,
    memory=0,
    memoryReservation=512,
    cpu=0,
    privileged=false,
    logDriverOptions=[
      new LogDriverOption('awslogs-group', env['AWS_LOGS_GROUP']),
      new LogDriverOption('awslogs-region', env['AWS_LOGS_REGION']),
      new LogDriverOption('awslogs-stream-prefix', env['AWS_LOGS_PREFIX']),
      // new LogDriverOption('awslogs-datetime-format', '[%b %d, %Y %H:%M:%S]')
    ],
    environments=[
      new EnvironmentEntry('JENKINS_S3_BUCKET_NAME', env['JENKINS_S3_BUCKET_NAME']),
      new EnvironmentEntry('JENKINS_SECURITY_GROUP', env['JENKINS_SECURITY_GROUP']),
      new EnvironmentEntry('JENKINS_SUBNET', env['JENKINS_SUBNET'])
    ],
    extraHosts=null,
    mountPoints=null
  )

  ecsTemplate.setLogDriver("awslogs")

  def ecsCloud = new ECSCloud(
    name=cloudName,
    ecsSlaveTemplates=[ecsTemplate],
    amazonEcsCredentials=null,
    ecsCluster=clusterArn,
    amazonEcsRegionName=region,
    alternativeJenkinsUrl="http://${masterIP}",
    ecsTaskCreationTimeout=120
  )

  clouds = Jenkins.getInstance().clouds
  clouds.add(ecsCloud)
  instance.save()
}
