#!groovy
import hudson.security.*
import jenkins.model.*

def env = System.getenv()
def instance = Jenkins.getInstance()

println "--> Creating Jenkins root user"

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount(env['JENKINS_ROOT_USERNAME'], env['JENKINS_ROOT_PASSWORD'])
instance.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()
