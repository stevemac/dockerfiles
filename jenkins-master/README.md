# Jenkins Master docker container


Use CloudFormation or Terraform to create an ECS cluster and specify an ECS Service.

Projected workflow:

* Create S3 bucket 'JENKINS_S3_BUCKET_NAME'
* Create config yaml file and push to S3
* Boot Master ECS container with specified environment variables
    * JENKINS_AGENT_CLUSTER_ARN = cluster ARN for launching task containers
    * JENKINS_S3_BUCKET_NAME = bucket to pull config and secrets from

This should boot the Jenkins Master, as part of the container start the entrypoint will configure the server.

`entrypoint.sh` will process the following:

* Validate the supplied parameters are correct
* Download config file from S3
* Process each section of the config file


## Sample config.yaml file
```
---
admin_username: "admin"
admin_password: "adminpassword"

dsl_jobs:
    - name: bootstrap_job
      jenkins_git_url: git@bitbucket.org:stevemac/bootstrap_dsl.git
      credential: basecredential

ecs_cluster_arn: <%= ENV('JENKINS_AGENT_CLUSTER_ARN') %>
ecs_templates:
    - template_name: base-agent
      label: base linux
      image: stevemac007/jenkins-base-agent:latest
      mount_points:
        - name: DockerSocket
          sourcePath: /var/run/docker.sock
          containerPath: /var/run/docker.sock
          readOnly: False
    - templateName: test-agent
      label: chromedriver linux
      image: stevemac007/jenkins-test-agent:latest

slack:
    team_domain: slack-team-name,
    token:
      s3_key: slack-token
      s3_bucket: <%= ENV['JENKINS_S3_BUCKET_NAME'] %>
      encrypted: true

git:
  username: CI/CD Server
  email: jenkins@somewhere.com

plugins:
    - "bitbucket"
    - "delivery-pipeline-plugin"
    - "pipeline-model-definition"
    - "job-dsl"
    - "build-pipeline-plugin"
    - "ansicolor"
    - "slack"
    - "amazon-ecs"
    - "ssh-credentials"
    - "rbenv"
    - "blueocean"
    - "envinject"
    - "jobcacher"

credentials:
    ssh:
        - username: basecredential
          id: basecredential
          description: Primary credential used for loading Jenkins job configurations
          s3_key: basecredential.pem
          s3_bucket: <%= ENV['JENKINS_S3_BUCKET_NAME'] %>

    secret_file:
        - id: aws-ssh-key
          description: SSH private key for access to the EC2 instances
          s3_key: jenkins-ssh-key.pem
          s3_bucket: <%= ENV['JENKINS_S3_BUCKET_NAME'] %>

    userpass:
        - id: artifactory
          description: Allows access to the Artifactory repository
          username: ciartifactory
          password:
            s3_key: artifactory_password
            s3_bucket: <%= ENV['JENKINS_S3_BUCKET_NAME'] %>
            encrypted: true
```
