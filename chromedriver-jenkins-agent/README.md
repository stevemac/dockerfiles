# Chrome driver Jenkins Agent

This is a Dockerfile to build a Jenkins agent ready with chrome driver installed.

It was needed to start with the `selenium/standalone-chrome` image
and then install the Kenkins slave agent.

Currently configured to deploy the Jenkins Slave v3.5.

Usage of this image is currently focused on supporting python behavioural tests using `behave`.

It would be easy to extend this image to pre-load other required behavioural test frameworks.

Additional tools installed to support tests are:
 * python
 * git
 * pip
