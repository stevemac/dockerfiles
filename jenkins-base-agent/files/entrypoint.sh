#!/usr/bin/env bash
set -x

echo "Bootstrapping environment configuration"

if [ -z $JENKINS_S3_BUCKET_NAME ]; then
    echo "ERROR: Cannot start Jenkins container without the JENKINS_S3_BUCKET_NAME set."
    exit 1
fi

mkdir /root/.ssh
chmod 600 /root/.ssh

aws configure set s3.signature_version s3v4
aws s3api get-object --bucket $JENKINS_S3_BUCKET_NAME --key id_rsa /root/.ssh/id_rsa

chmod 600 /root/.ssh/id_rsa

# Now its time to launch jenkins slave
/usr/local/bin/jenkins-slave "$@"
