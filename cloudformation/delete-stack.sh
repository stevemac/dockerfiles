set -e
STACK_NAME=jenkins-as-code

stackup ${STACK_NAME}-services delete
stackup ${STACK_NAME} delete
