set -e
STACK_NAME=jenkins-as-code
stackup ${STACK_NAME} up -t jenkins_infra.yaml -p parameters.yaml -o KeyName=stevemac-cevodev

stackup ${STACK_NAME}-services up -t jenkins_cluster.yaml -p cluster_parameters.yaml -o MasterDNSName=jenkins-as-code
