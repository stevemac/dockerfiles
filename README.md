# Jenkins as Code demonstration

This repository contains working examples of a Jenkins as Code approach to delivering a CI/CD cluster on AWS using ECS.

## CloudFormation

The `cloudformation` folder contains 2 CloudFormation templates that can be used to create and ECS cluster, and then deploy an ECS service containing the customised Jenkins Master container.

## Jenkins Master

The `jenkins-master` folder contains the Dockerfile published to `https://hub.docker.com/r/stevemac007/jenkins-master/` and contains some customisations on top of the standard `jenkinsci/jenkins` container.

This image creates a custom entry point that configures some groovy scripts to execute at Jenkins launch time.

These scripts set up a default Administration user, enabled the configuration for the ECS task plugin and an initial example seed job.

## Jenkins Base Agent

The `jenkins-base-agent` folder contains the Dockerfile published to `https://hub.docker.com/r/stevemac007/jenkins-base-agent/` and contains build tools on top of the standard `jenkinsci/slave` image

## Chromedriver Jenkins Agent

The `chromedriver-jenkins-agent` folder contains a Dockerfile not currently published anywhere,
 it extends the `selenium/standalone-chrome` container and configures it as a jenkins-slave.
